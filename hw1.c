#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <regex.h>

const int USER_REQUIRED = 1;
const int PASSWORD_REQUIRED = 0;
const int EMAIL_REQUIRED = 0;

char *getArgumentByKey(int argc, char** args, char *key) {

  for(int index = 1; index < argc; index++) {
    if(strcmp(args[index], key) == 0) {
      // The key was found, we get the argument. (if there is one)
      char *argument = malloc(sizeof(argc));
      if(args[index+1] != NULL) {
        strcpy(argument, args[index+1]);
        return argument;
      } else {
        return NULL;
      }
    }
  }
  return NULL;
}

int isDotAtStart(char *string) {
  if(string[0] == '.') {
    return 1;
  }
  return 0;
}

int isDotAtEnd(char *string) {
  if(string[strlen(string)-1] == '.') {
    return 1;
  }
  return 0;
}

int doesDotAppearConsecutively(char *string) {
  for(int index = 0; index < strlen(string); index++) {
    if(string[index] == '.') {
      if(string[index+1] == '.') {
        return 1;
      }
    }
  }
  return 0;
}

int isUserValid(char *user) {
  int onlyAlphanumeric = 0;

  regex_t regex;
  int reg;

  reg = regcomp(&regex, "^[a-zA-Z0-9_]*$", 0);
  reg = regexec(&regex, user, 0, NULL, 0);
  if(!reg) {
    onlyAlphanumeric = 1;
  }

  regfree(&regex);

  int isValid = (onlyAlphanumeric);
  return isValid;
}

int isEmailValid(char *email) {
  int isAtCharacterPresent = 0;
  int noDotAtStart = 0;
  int noDotAtEnd = 0;
  int noDotConsecutively = 0;

/*  The local-part of the e-mail address may use any of these ASCII characters:

  Uppercase and lowercase English letters (a-z, A-Z)
  Digits 0 to 9
  Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~
  Character . (dot, period, full stop) provided that it is not the first or last character,
  and provided also that it does not appear two or more times consecutively.
*/

  char localPart[50];
  char domainPart[50];

  for(int index = 0; index <= strlen(email); index++) {
    if(email[index] == '@') {
      isAtCharacterPresent = 1;
      strncpy(localPart, email, index);
      localPart[index] = '\0'; // add null terminator as strncpy won't do it
      strncpy(domainPart, &email[index+1], strlen(email));
      domainPart[strlen(email) - (index+1)] = '\0';
    }
  }

  if(isAtCharacterPresent) {
    int noDotAtStartLocalPart = !isDotAtStart(localPart);
    int noDotAtStartDomainPart = !isDotAtStart(domainPart);
    noDotAtStart = (noDotAtStartLocalPart && noDotAtStartDomainPart);

    int noDotAtEndLocalPart = !isDotAtEnd(localPart);
    int noDotAtEndDomainPart = !isDotAtEnd(domainPart);
    noDotAtEnd = (noDotAtEndLocalPart && noDotAtEndDomainPart);

    int noDotConsecutivelyLocalPart = !doesDotAppearConsecutively(localPart);
    int noDotConsecutivelyDomainPart = !doesDotAppearConsecutively(domainPart);
    noDotConsecutively = (noDotConsecutivelyLocalPart && noDotConsecutivelyDomainPart);
  }

  int valid = (isAtCharacterPresent &&
               noDotAtStart &&
               noDotAtEnd &&
               noDotConsecutively);
  return valid;
}

int isPasswordValid(char *password) {
  int hasUppercase = 0;
  int minCharacters = 0;

  if(strlen(password) >= 3) {
    minCharacters = 1;
  } else {
    puts("Password is too short!");
  }

  for(int index = 0; index < strlen(password); index++) {
    if(isupper(password[index])) {
      hasUppercase = 1;
    }
  }

  if(!hasUppercase) {
    puts("Password is insecure!");
  }

  int valid = (hasUppercase && minCharacters);
  return valid;

}

int argumentsValid(int argc, char** args) {
  int argumentsValid = 1;

  if(USER_REQUIRED) {
    char *argument = getArgumentByKey(argc, args, "-u");
    if(argument == NULL) {
      puts("User is required.");
      argumentsValid = 0;
    }
    free(argument);
  }

  if(EMAIL_REQUIRED) {
    char *argument = getArgumentByKey(argc, args, "-e");
    if(argument == NULL) {
      puts("Email is required.");
      argumentsValid = 0;
    }
    free(argument);
  }

  if(PASSWORD_REQUIRED) {
    char *argument = getArgumentByKey(argc, args, "-p");
    if(argument == NULL) {
      puts("Password is required.");
      argumentsValid = 0;
    }
    free(argument);
  }

  char *user = getArgumentByKey(argc, args, "-u");
  if(user != NULL) {
    if(!isUserValid(user)) {
      puts("User is not valid!");
      argumentsValid = 0;
    }
  }
  free(user);

  char *email = getArgumentByKey(argc, args, "-e");
  if(email != NULL) {
    if(!isEmailValid(email)) {
      puts("E-mail is not valid!");
      argumentsValid = 0;
    }
  }
  free(email);

  char *password = getArgumentByKey(argc, args, "-p");
  if(password != NULL) {
    if(!isPasswordValid(password)) {
      argumentsValid = 0;
    }
  }

  free(password);

  return argumentsValid;
}

int main(int argc, char** args) {

  if(argumentsValid(argc, args)) {
    char *user = getArgumentByKey(argc, args, "-u");
    char *email = getArgumentByKey(argc, args, "-e");
    char *password = getArgumentByKey(argc, args, "-p");

    if(password == NULL && email != NULL && user != NULL) {
      printf("User %s has email %s\n", user, email);
      exit(1);
    }
    if(password != NULL && email != NULL && user != NULL) {
      printf("User %s has email %s and the password %s\n", user, email, password);
      exit(1);
    }
    if(password == NULL && email == NULL && user != NULL) {
      printf("Howdy %s!\n", user);
      exit(1);
    }
  }
  exit(0);
}
