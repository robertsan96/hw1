* REQUIREMENTS **
This homework involves creating a simple bash script & C program which is entitled "hw1" and is called as per the following syntax:

$ ./hw1 -u <user> -p <password> -e <email>

Your program should perform the following checks:
a) the "-u" argument is mandatory and requires the <user> to follow immediately
b) the "-p" argument is optional, but if present requires the <password> to follow immediately
c) the "-e" argument is optional also, but if present requires the <email> to follow immediately
d) the <email> should be verified to comply to the email naming conventions (e.g. "andrei.test@test.com" is valid, but "andrei..ion@test.com" is not valid, etc.)
e) arguments may be in any order, not necessarily in the order given above
f) upon execution, the following need to happen:
f.1) if the password is shorter than 3 characters, program needs to show "Password is too short!" and terminate
f.2) if the password does not contain an uppercase letter, program needs to show "Password is insecure!" and terminate
f.3) if the user contains any characters other than letters and numbers, program needs to show "Username is not valid!" and terminate
f.4) if the email does is invalid, program needs to show "E-mail is not valid!" and terminate
f.5) if the password is not present in the arguments, program needs to show "User <user> has email <email>" and terminate.
f.6) if the password is present, program needs to show "User <user> has the email <email> and the password <password>" and terminate.

** IMPORTANT PRACTICAL & ETHICAL GUIDELINES **
1. Your final homework delivery should include both a C program and a bash script to resolve the task required. Both should be identical in functionality.
2. Each week of delay means 2 additional points dropped from your maximum possible grade.
3. All homeworks submitted are being checked for plagiarism using MOSS. Two consecutive plagiarisms from the same student determine immediate failure of the lab and course.
